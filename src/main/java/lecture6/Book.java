package lecture6;

import java.util.ArrayList;

public class Book {
	//field variables
	ArrayList<String> authors;
	int numPages;
	String title;
	
	//Constructor
	public Book(String author, int pages, String title){
		this(toList(author),pages,title);
	}

	public Book(ArrayList<String> authors, int pages, String title){
		this.authors = new ArrayList<String>();
		this.authors = authors;
		numPages = pages;
		this.title = title;
	}

	//helper methods
	private static ArrayList<String> toList(String author){
		ArrayList<String> authors = new ArrayList<String>();
		authors.add(author);
		return authors;
	}
	
	//methods
	public String getFirstAuthor() {
		return authors.get(0);
	}
	
	public void tearOutPage() {
		this.numPages--;
	}
	
	@Override
	public String toString() {
		return "Title: "+this.title+" by "+this.authors.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if( obj instanceof Book) {
			Book book = (Book)obj;
			return equals(book);
		}
		return false;
	}

	public boolean equals(Book other) {
		if(!this.title.equals(other.title)) {
			return false;
		}
		if(this.numPages != other.numPages) {
			return false;
		}
		if(!this.authors.equals(other.authors)) {
			return false;
		}
		return true;
	}
}
