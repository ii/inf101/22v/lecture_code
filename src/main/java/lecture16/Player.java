package lecture16;

public interface Player {

	public Move getMove(Game game);

}
