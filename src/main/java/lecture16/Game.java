package lecture16;

public interface Game {

	boolean isOver();

	Player getCurrentPlayer();

	boolean isValid(Move move);

	void makeMove(Move move);

}
