package lecture16;

public class Feilmeldinger {

	public static void main(String[] args) {

		//nullPointer();
		//indexOutOfBounds();
		printReverse("Martin");
		printReverse("");
	}

	private static void printReverse(String string) {
		if(string.length()==1)
			System.out.print(string);
		else {
			printReverse(string.substring(1));
			System.out.print(string.charAt(0));
		}
		
	}

	private static void indexOutOfBounds() {
		int[] tall = new int[]{1,2,3,4};
		for(int i=1; i<=4; i++) {
			System.out.println(tall[i]);
		}
		
	}

	private static void nullPointer() {
		String name = null;
		int tall = 42;
		if(tall>42) {
			name = "Martin";
		}
		for(int i=0; i<name.length(); i++)
			System.out.println(name.charAt(i));
	}

}
