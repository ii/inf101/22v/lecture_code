package lecture16;

public class GameLoop {

	public void run(Game game){
		
		while(!game.isOver()) {
			Player player = game.getCurrentPlayer();
			Move move = player.getMove(game);
			if(game.isValid(move)) {
				game.makeMove(move);
			}
			else
				System.out.println("InvalidMove");
		}
	}
}
