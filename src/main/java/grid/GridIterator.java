package grid;

import java.util.Iterator;

public class GridIterator implements Iterator<Location> {

	int rows;
	int cols;
	Location next;
	
	GridIterator(int rows, int cols){
		this.rows = rows;
		this.cols = cols;
		next = new Location(0, 0);
	}
	@Override
	public boolean hasNext() {
		return isOnGrid(next);
	}
	
	private boolean isOnGrid(Location loc) {
		if(loc.row < rows && loc.col<cols)
			return true;
		else
			return false;
	}

	@Override
	public Location next() {
		Location toReturn = next;
		next = GridDirection.getNeighbour(next, GridDirection.EAST);
		if(!isOnGrid(next)) {
			next = new Location(next.row+1,0);
		}
			
		return toReturn;
	}

}
