package grid;

public enum GridDirection {

	NORTH(-1,0),
	SOUTH(+1,0),
	WEST(0,-1),
	EAST(0,+1);
	
	int dRow;
	int dCol;
	
	private GridDirection(int dRow, int dCol) {
		this.dCol = dCol;
		this.dRow = dRow;
	}
	
	public static Location getNeighbour(Location loc, GridDirection dir){
		return new Location(loc.row+dir.dRow, loc.col+dir.dCol);
	}
}
