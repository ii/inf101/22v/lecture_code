package grid;

import java.util.Iterator;

public class GridIterable implements Iterable<Location> {

	int rows;
	int cols;
	
	public GridIterable(int rows, int cols) {
		this.rows = rows;
		this.cols = cols;
	}

	@Override
	public Iterator<Location> iterator() {
		return new GridIterator(rows,cols);
	}

}
